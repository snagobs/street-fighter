class Fighter {
    _id;
    _name;
    _health;
    _attack;
    _defense;
    _source;

    constructor(data) {
        Object.assign(this, data);
    }

    get health() {
        return this._health;
    }

    set health(value) {
        this._health = value;
    }

    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get attack() {
        return this._attack;
    }

    set attack(value) {
        this._attack = value;
    }

    get defense() {
        return this._defense;
    }

    set defense(value) {
        this._defense = value;
    }

    get source() {
        return this._source;
    }

    set source(value) {
        this._source = value;
    }

    get getHitPower() {
        function criticalHitChance(min, max) {
            return Math.random() * (max - min) + min;
        }

        let hitPower = Math.round(this._attack * criticalHitChance(1, 2));
        return hitPower;
    }

    get getBlockPower() {
        function dodgeChance(min, max) {
            return Math.random() * (max - min) + min;
        }

        let blockPower = Math.round(this._defense * dodgeChance(1, 2));
        return blockPower;
    }

}

export default Fighter;