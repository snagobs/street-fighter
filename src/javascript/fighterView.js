import View from './view';

class FighterView extends View {
    constructor(fighter, handleClick) {
        super();

        this.createFighter(fighter, handleClick);
    }

    createFighter(fighter, handleClick) {
        const {name, source, _id} = fighter;
        const nameElement = this.createName(name);
        const imageElement = this.createImage(source);
        const checkFighterElement = this.createCheckbox(_id);

        this.fighterElement = this.createElement({tagName: 'div', className: 'fighter'});
        this.fighterDetailsElement = this.createElement({tagName: 'div', className: 'fighter-details'});
        this.fighterDetailsElement.append(imageElement, nameElement);
        this.fighterElement.append(this.fighterDetailsElement, checkFighterElement);
        this.fighterDetailsElement.addEventListener('click', event => handleClick(event, fighter), false);
        this.element = this.fighterElement;
        
    }

    createName(name) {
        const nameElement = this.createElement({tagName: 'span', className: 'name'});
        nameElement.innerText = name;
        
        return nameElement;
    }

    createImage(source) {
        const attributes = {src: source};
        const imgElement = this.createElement({
            tagName: 'img',
            className: 'fighter-image',
            attributes
        });

        return imgElement;
    }

    createCheckbox(id) {
        const attributes = {value: id, type: "checkbox"};
        const checkboxlement = this.createElement({
            tagName: 'input',
            className: 'fighter-select',
            attributes
        });
        return checkboxlement;
    }
}

export default FighterView;