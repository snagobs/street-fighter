import View from './view';
import Fighter from './Fighter';
import FighterView from './fighterView';
import {fighterService} from './services/fightersService';

class FightersView extends View {
    constructor(fighters) {
        super();

        this.handleClick = this.handleFighterClick.bind(this);
        this.createFighters(fighters);

    }

    fightersDetailsMap = new Map();

    createFighters(fighters) {
        const fighterElements = fighters.map(fighter => {
            const fighterView = new FighterView(fighter, this.handleClick);
            return fighterView.element;
        });

        const startFightBtn = this.createStartFightBtn();
        startFightBtn.addEventListener('click', event => this.startFightClick(event), false);

        this.element = this.createElement({tagName: 'div', className: 'fighters'});
        this.element.append(...fighterElements, startFightBtn);
    }

    async startFightClick(event) {

        try {
            const fighters = document.getElementsByClassName("fighter-select");

            let ids = [];

            for (let fighter of fighters) {
                if (fighter.checked) {

                    ids.push(fighter.value);
                }
            }
            const f1 = new Fighter(await fighterService.getFighterDetails(ids[0]));
            const f2 = new Fighter(await fighterService.getFighterDetails(ids[1]));
            await fighterService.fight(f1, f2);

        } catch (error) {
            throw error;
        }

    }

    createStartFightBtn() {
        let btn = document.createElement("BUTTON",);
        btn.innerHTML = "START FIGHT!";
        return btn;
    }

    async handleFighterClick(event, fighter) {
        this.fightersDetailsMap.set(fighter._id, fighter);
        showModalWin();

        const fighterDetails = await fighterService.getFighterDetails(fighter._id);

    }
}

export default FightersView;