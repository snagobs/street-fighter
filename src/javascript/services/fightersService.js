import {callApi} from '../helpers/apiHelper';

class FighterService {
    async getFighters() {
        try {
            const endpoint = 'fighters.json';
            const apiResult = await callApi(endpoint, 'GET');
            return JSON.parse(atob(apiResult.content));
        } catch (error) {
            throw error;
        }
    }

    async getFighterDetails(_id) {
        try {
            const endpoint = 'details/fighter/' + _id + '.json';
            const apiResult = await callApi(endpoint, 'GET');
            return JSON.parse(atob(apiResult.content));
        } catch (error) {
            throw error;
        }
    }

    async fight(fighter1, fighter2) {

        function fighterOneDamageCount() {
            let fighterOneGetDamage = fighter2.getHitPower - fighter1.getBlockPower;
            if (fighterOneGetDamage < 0) {
                fighterOneGetDamage = 0;
            }
            return fighterOneGetDamage;
        }

        function fighterTwoDamageCount() {
            let fighterTwoGetDamage = fighter1.getHitPower - fighter2.getBlockPower;
            if (fighterTwoGetDamage < 0) {
                fighterTwoGetDamage = 0;
            }
            return fighterTwoGetDamage;
        }

        for (; fighter1.health > 0 || fighter2.health > 0;) {
            fighter1.health = fighter1.health - fighterOneDamageCount();
            fighter2.health = fighter2.health - fighterTwoDamageCount();

            if (fighter1.health <= 0) {
                alert(fighter2.name + " WIN! Flawless victory!!!");
                break;
            }
            if (fighter2.health <= 0) {
                alert(fighter1.name + " WIN! Amazing battle!!!");
                break;
            }
        }        
    }
}

export const fighterService = new FighterService();
